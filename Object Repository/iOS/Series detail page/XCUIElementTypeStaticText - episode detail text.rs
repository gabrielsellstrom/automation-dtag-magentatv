<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>XCUIElementTypeStaticText - episode detail text</name>
   <tag></tag>
   <elementGuidId>00000000-0000-0000-0000-000000000000</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>XCUIElementTypeStaticText</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>enabled</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>242.0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>label</name>
      <type>Main</type>
      <value>Der Farmer ist entzückt über den Besuch seiner Lieblingsnichte und legt sich mächtig mit Tortebacken ins Zeug. Sobald er dem Mädchen den Rücken zudreht, zeigt sie ihr wahres Gesicht. Sie ist eine freche Göre, die immer ihren Kopf durchsetzten will. Das bekommen die Schafe zu spüren, die sich die Nichte als Spielobjekte ausgesucht hat. Als der Farmer der Kleinen die Torte präsentieren will, landet das Mädchen in eben dieser.</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Der Farmer ist entzückt über den Besuch seiner Lieblingsnichte und legt sich mächtig mit Tortebacken ins Zeug. Sobald er dem Mädchen den Rücken zudreht, zeigt sie ihr wahres Gesicht. Sie ist eine freche Göre, die immer ihren Kopf durchsetzten will. Das bekommen die Schafe zu spüren, die sich die Nichte als Spielobjekte ausgesucht hat. Als der Farmer der Kleinen die Torte präsentieren will, landet das Mädchen in eben dieser.</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Der Farmer ist entzückt über den Besuch seiner Lieblingsnichte und legt sich mächtig mit Tortebacken ins Zeug. Sobald er dem Mädchen den Rücken zudreht, zeigt sie ihr wahres Gesicht. Sie ist eine freche Göre, die immer ihren Kopf durchsetzten will. Das bekommen die Schafe zu spüren, die sich die Nichte als Spielobjekte ausgesucht hat. Als der Farmer der Kleinen die Torte präsentieren will, landet das Mädchen in eben dieser.</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>visible</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>686.0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x</name>
      <type>Main</type>
      <value>32.0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>y</name>
      <type>Main</type>
      <value>920.0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeCollectionView[1]/XCUIElementTypeCell[17]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[3]</value>
   </webElementProperties>
</WebElementEntity>
