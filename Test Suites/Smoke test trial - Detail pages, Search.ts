<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Smoke test trial - Detail pages, Search</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a9ee734e-3543-4d22-a5bf-38639a31b095</testSuiteGuid>
   <testCaseLink>
      <guid>d71155cf-5a0e-4953-89e6-f6e0526ff664</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>622f5003-1c33-4d21-be85-ceffa2da9f82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Open Movie detail page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25c1833a-2658-4f0f-970d-2b5b2df21b75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Open Series Detail page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc5fac9b-7eab-41d0-8f72-04ba14a7f8f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Open Musik Detail page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>986e59b6-ec36-4953-9455-ffddc5373df0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Open Sport Detail page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
