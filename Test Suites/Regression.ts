<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8c4adc46-fbd0-4803-acc4-8dbac0234672</testSuiteGuid>
   <testCaseLink>
      <guid>0ebf4b4b-5162-4c8a-ae8f-bada2e1647ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login wrong credentials</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6709c77-1fde-42d1-bc64-6132571f5684</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login info button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aab76ed2-a00d-450f-9789-88b4eff5148a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login button state</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>619004a2-bc4a-49aa-9b7d-9f9bdf5e106a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Login D2 user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f44ca20-d3da-458e-8e49-34f6a3d64c46</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LoginLogout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>873536c1-a9da-4d0b-bd9b-2a856de5ed23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LoginLogout Persistent</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d43a0675-e606-4cbe-b7e9-04afe103508a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>689536f8-c85d-4ed1-9fa6-c3680eee3b3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Open all menu items</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34fb780f-cbd4-4533-8f0e-e1201a638b69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7cd51561-8aa1-4996-9ffa-5587e24536cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/EPG</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4010de8e-ffaf-4e45-ba4e-dadbb8dd65f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Verify landing page</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ffa675b4-a4a1-47e7-a03b-dc270b50dbd9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e8e06e64-0e29-4407-b34c-5ddfa7c544ce</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0fefef1c-3a3b-44e4-82ab-348f93b79ffc</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
