import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String platform = GlobalVariable.PLATFORM

Mobile.tap(findTestObject(platform + '/Menu/ActionBarTab1'), 0)

Mobile.setText(findTestObject(platform + '/Search/XCUIElementTypeTextField - Search field'), 'Shaun', 0)

Mobile.tap(findTestObject(platform + '/Search/XCUIElementTypeButton - Search button'), 0)

Mobile.tap(findTestObject(platform + '/Series detail page/XCUIElementTypeStaticText - Shaun das Schaf'), 0)

Mobile.waitForElementPresent(findTestObject(platform + '/Series detail page/XCUIElementTypeStaticText - Alle Episoden herunterladen'), 
    0)

Mobile.tap(findTestObject(platform + '/Series detail page/XCUIElementTypeStaticText - STAFFEL 1'), 0)

if (platform == 'iOS') {
    CustomKeywords.'ios_custom.ioskeywords.getCurrentSessionMobileDriver'()

    CustomKeywords.'ios_custom.ioskeywords.swipeDirection'('up')
} else {
    Mobile.scrollToText('', FailureHandling.STOP_ON_FAILURE)
}

Mobile.waitForElementPresent(findTestObject(platform + '/Series detail page/XCUIElementTypeStaticText - 32. Die Nichte des Farmers'), 
    0)

Mobile.tap(findTestObject(platform + '/Series detail page/XCUIElementTypeStaticText - 32. Die Nichte des Farmers'), 0)

Mobile.waitForElementPresent(findTestObject(platform + '/Series detail page/XCUIElementTypeButton - action button cart'), 0)

Mobile.takeScreenshot('/users/tamas.groholy/Desktop/ios_test.png', FailureHandling.STOP_ON_FAILURE)

