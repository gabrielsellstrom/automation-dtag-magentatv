import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String platform = GlobalVariable.PLATFORM

Mobile.tap(findTestObject(platform + '/Menu/ActionBarTab1'), 0)

Mobile.waitForElementPresent(findTestObject(platform + '/Menu/search field'), 10)

Mobile.setText(findTestObject(platform + '/Menu/search field'), 'multi partner movie', 5)

Mobile.tap(findTestObject(platform + '/Search/XCUIElementTypeButton - Search button'), 0)

Mobile.waitForElementPresent(findTestObject(platform + '/Movie detail page/XCUIElementTypeStaticText - Movie multi Partner  Rent'), 
    1)

Mobile.tap(findTestObject(platform + '/Movie detail page/XCUIElementTypeStaticText - Movie multi Partner  Rent'), 0)

if (platform == 'Android') {
    Mobile.scrollToText('Director', FailureHandling.STOP_ON_FAILURE)
}

Mobile.takeScreenshot('', FailureHandling.STOP_ON_FAILURE)

