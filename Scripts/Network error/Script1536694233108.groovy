import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String platform = GlobalVariable.PLATFORM

WebUI.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject(platform + '/Menu/ActionBarTab3'), 0)

Mobile.toggleAirplaneMode('on', FailureHandling.STOP_ON_FAILURE)

Mobile.comment('Not allowed on Android 7+')

Mobile.tap(findTestObject(platform + '/Menu/TV Programm'), 0)

Mobile.verifyElementVisible(findTestObject('null'), 0)

Mobile.verifyElementText(findTestObject('null'), 'Error')

Mobile.verifyElementText(findTestObject('null'), 'Could not find any content for the given page.')

Mobile.pressBack()

Mobile.verifyElementVisible(findTestObject(platform + '/Error message/TRY AGAIN'), 0)

Mobile.tap(findTestObject(platform + '/Error message/TRY AGAIN'), 0)

Mobile.verifyElementVisible(findTestObject(platform + '/Error message/EXIT APP'), 0)

Mobile.tap(findTestObject(platform + '/Error message/EXIT APP'), 0)

Mobile.toggleAirplaneMode('off', FailureHandling.STOP_ON_FAILURE)

Mobile.comment('W.I.P - Not working atm')

