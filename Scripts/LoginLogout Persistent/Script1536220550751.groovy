import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String platform = GlobalVariable.PLATFORM

Mobile.startApplication(GlobalVariable.path, false)

Mobile.tap(findTestObject(platform + '/Loginreg/LOGIN'), 60)

Mobile.setText(findTestObject(platform + '/Loginreg/username'), GlobalVariable.username, 0)

Mobile.setText(findTestObject(platform + '/Loginreg/password'), GlobalVariable.password, 0)

Mobile.checkElement(findTestObject(platform + '/Loginreg/Stay signed in'), 0)

Mobile.tap(findTestObject(platform + '/Loginreg/LOGINbutton'), 0)

Mobile.tap(findTestObject(platform + '/Menu/ActionBarTab3'), 0)

Mobile.scrollToText('Logout', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject(platform + '/Menu/Logoutmenu'), 0)

Mobile.tap(findTestObject('Android/Menu/Logout/OK'), 0)

Mobile.waitForElementPresent(findTestObject(platform + '/Loginreg/REGISTER NOW'), 0)

Mobile.closeApplication()

