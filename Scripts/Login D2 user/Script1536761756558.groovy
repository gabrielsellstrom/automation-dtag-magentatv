import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String platform = GlobalVariable.PLATFORM

Mobile.startApplication(GlobalVariable.path, false)

Mobile.tap(findTestObject(platform + '/Loginreg/LOGIN'), 60)

'REGISTERED_ONLY_DT user'
Mobile.setText(findTestObject(platform + '/Loginreg/username'), '550214003457', 0)

Mobile.setText(findTestObject(platform + '/Loginreg/password'), GlobalVariable.password, 0)

Mobile.tap(findTestObject(platform + '/Loginreg/LOGINbutton'), 0)

Mobile.verifyElementVisible(findTestObject(platform + '/No rights popup/NoRightsPopup'), 0)

Mobile.verifyElementText(findTestObject(platform + '/No rights popup/PopupTitle'), 'No rights')

Mobile.verifyElementText(findTestObject(platform + '/No rights popup/PopupMsg'), 'You don\'t have access to the product, please upgrade your package through this link')

Mobile.tap(findTestObject(platform + '/No rights popup/OKbutton'), 0)

Mobile.verifyElementNotVisible(findTestObject(platform + '/No rights popup/NoRightsPopup'), 0)

Mobile.verifyElementNotVisible(findTestObject(platform + '/No rights popup/PopupTitle'), 0)

Mobile.verifyElementNotVisible(findTestObject(platform + '/No rights popup/PopupMsg'), 0)

Mobile.verifyElementVisible(findTestObject(platform + '/Loginreg/LOGINbutton'), 0)

