package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p>Profile default : app path apk
Profile Android : app path apk</p>
     */
    public static Object path
     
    /**
     * <p>Profile default : IPTV_OTT_DT user
Profile Android : IPTV_OTT_DT user</p>
     */
    public static Object username
     
    /**
     * <p>Profile default : password for login
Profile Android : password for login</p>
     */
    public static Object password
     
    /**
     * <p></p>
     */
    public static Object PLATFORM
     

    static {
        def allVariables = [:]        
        allVariables.put('default', ['path' : '/Users/gabriel/Downloads/app-debug.apk', 'username' : '550214003434', 'password' : '37947304'])
        allVariables.put('Android', allVariables['default'] + ['path' : '/Users/gabriel/Downloads/app-debug.apk', 'username' : '550214003434', 'password' : '37947304', 'PLATFORM' : 'Android'])
        allVariables.put('iOS', allVariables['default'] + ['username' : '550214003434', 'password' : '37947304', 'PLATFORM' : 'iOS', 'path' : '/Users/tamas.groholy/Downloads/MTV_MOBILE_DEVELOP-061.ipa'])
        
        String profileName = RunConfiguration.getExecutionProfile()
        
        def selectedVariables = allVariables[profileName]
        path = selectedVariables['path']
        username = selectedVariables['username']
        password = selectedVariables['password']
        PLATFORM = selectedVariables['PLATFORM']
        
    }
}
