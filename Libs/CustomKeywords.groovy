
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String


def static "ios_custom.ioskeywords.swipeDirection"(
    	String direction	) {
    (new ios_custom.ioskeywords()).swipeDirection(
        	direction)
}

def static "ios_custom.ioskeywords.getCurrentSessionMobileDriver"() {
    (new ios_custom.ioskeywords()).getCurrentSessionMobileDriver()
}
